from blog.models import *
from django.contrib import admin
from django.forms import TextInput, Textarea
# from tinymce.widgets import TinyMCE

from django import forms



# class blog_modaladmin(admin.ModelAdmin):
# 	formfield_overrides = {
# 					models.TextField: {'widget':TinyMCE(attrs={'rows':30, 'cols':120})},
# 					}


class BlogAdminForm(forms.ModelForm):
    # blog_data = forms.CharField(widget=TinyMCE(attrs={'cols': 150, 'rows': 40}))
    blog_data = forms.CharField(widget=Textarea(attrs={'cols': 150, 'rows': 40}))

    class Meta:
        model = blog
        fields = "__all__"


class BlogAdmin(admin.ModelAdmin):
    form = BlogAdminForm
    search_fields = ['blog_title']


admin.site.register(blog_files)
# admin.site.register(blog,blog_modaladmin)
admin.site.register(blog, BlogAdmin)