from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from blog.models import *
import re
from blog.forms import DocumentForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.http import Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

ITEMS_PER_PAGE=20

def fileupload(request):
	if request.user.is_staff:
		pass
	else:
		return HttpResponse('login first')
	if request.method=='POST':
		form=DocumentForm(request.POST,request.FILES)
		if form.is_valid():
			newdoc=blog_files(docfile=request.FILES['docfile'])
			newdoc.save()

			return HttpResponseRedirect(reverse('blog.views.fileupload'))
	else:
		form=DocumentForm()


	documents=blog_files.objects.all()[::-1]

	try:
		if request.GET['p']:
			page_number=int(request.GET['p'])
		else:
			page_number=1
	except Exception, e:
		page_number=1
	page=page_number

	paginator = Paginator(documents,ITEMS_PER_PAGE)


	try:
		documents = paginator.page(page)
		docs = []
		for doc in documents:
			doc_item={}
			doc_item['name']=doc.docfile.name
			doc_item['url']=doc.docfile.url
			day = doc.date
			doc_item['month']=day.strftime("%B")
			doc_item['year']=doc.date.year
			doc_item['day']=doc.date.day
			docs.append(doc_item)

	except PageNotAnInteger:
		documents = paginator.page(1)
	except EmptyPage:
		documents = paginator.page(paginator.num_pages)

	return render_to_response(
		'blog/templates/fileupload.html',
		{'documents':documents,
		'docs':docs,
		'form':form,
		'page_list':documents,
		},
		context_instance=RequestContext(request)
		)