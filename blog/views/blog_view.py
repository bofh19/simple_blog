from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from blog.models import *
import re

def blog_detail(request,blog_id):
	this_blog=blog.objects.get(id=blog_id)
	url_title = re.sub(' ','-',this_blog.blog_title)
	if this_blog.blog_enabled == '0':
		this_blog=''
		url_title=''
	return render_to_response('blog/templates/blog_detail.html',
								{
								'blog':this_blog,
								'url_title':url_title,
								},context_instance=RequestContext(request)
							)
