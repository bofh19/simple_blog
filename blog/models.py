from django.db import models

# Create your models here.

import datetime
import uuid
# Create your models here.
import os


class blog(models.Model):
    blog_enabled_choices = ((u'0', u'Disabled'), (u'1', u'Enabled'))
    blog_title = models.CharField(max_length=200)
    blog_date = models.DateField(auto_now=True)
    blog_time = models.TimeField(auto_now=True)
    blog_data = models.TextField(blank=True)
    blog_enabled = models.CharField(max_length=1, choices=blog_enabled_choices, default='1')

    def __unicode__(self):
        return self.blog_title


def get_file_path(instance, filename):
    return os.path.join(instance.directory_string_var, filename)


class blog_files(models.Model):
    docfile = models.FileField(upload_to=get_file_path)
    date = models.DateField(auto_now=True)
    now = datetime.datetime.now()
    image_desc = models.CharField(max_length=200, blank=True, default='none')
    directory_string_var = 'site-media/blog_images/%s/%s/%s/' % (now.year, now.month, now.day)

    def __unicode__(self):
        return str(self.id) + "------" + self.image_desc
