from django.conf.urls import patterns, include, url

urlpatterns = patterns('blog.views',
    url(r'^(?P<blog_id>\d+)\-?(.?)+/$','blog_detail',name='blog_detail'),

     url(r'^fileupload/$', 'fileupload',name='fileupload'),
)
