# this_about = about_model.objects.all()[0]

from django import template
from index.models import title as title_model

register = template.Library()

@register.filter
def get_title(parser):
	try:
		title = title_model.objects.all()[0]
		this_title = title.blog_title
	except Exception, e:
		this_title='No title'
		
	return this_title

# @register.filter
# def upcomming_count(parser):
# 	movies_in_theater=movie_theater_status.objects.filter(release_status='D')
# 	return movies_in_theater.count()