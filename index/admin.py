from index.models import *
from django.contrib import admin
from django.forms import TextInput, Textarea

from django import forms

# class blog_modaladmin(admin.ModelAdmin):
# formfield_overrides = {
# models.TextField: {'widget':TinyMCE(attrs={'rows':30, 'cols':120})},
# }


class AboutAdminForm(forms.ModelForm):
    # blog_data = forms.CharField(widget=TinyMCE(attrs={'cols': 150, 'rows': 40}))
    about_data = forms.CharField(widget=Textarea(attrs={'cols': 150, 'rows': 40}))

    class Meta:
        model = about
        fields = "__all__"


class AboutAdmin(admin.ModelAdmin):
    form = AboutAdminForm

# admin.site.register(blog,blog_modaladmin)
admin.site.register(about, AboutAdmin)
admin.site.register(title)