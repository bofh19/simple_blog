from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from index.models import about as about_model

def about(request):
	try:
		this_about = about_model.objects.all()[0]
	except Exception, e:
		print e
		this_about=''
	return render_to_response('index/templates/about.html',
								{
								'about':this_about
								},context_instance=RequestContext(request)
							)
