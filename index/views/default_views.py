from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from blog.models import *
import re
from bs4 import BeautifulSoup
from bs4 import UnicodeDammit

MAX_TEXT_LEN=2000
MAX_NUM_IMAGES=4
def main_page(request):
	all_blogs=[]
	blog_all = blog.objects.all()[::-1]
	docs=[]
	for each_blog in blog_all:
		if each_blog.blog_enabled == '1':
			doc_item={}
			doc_item['blog_title'] = each_blog.blog_title
			doc_item['id'] = each_blog.id
			doc_item['url_title'] = re.sub(' ','-',each_blog.blog_title)
			day = each_blog.blog_date
			doc_item['blog_date'] = each_blog.blog_date
			doc_item['month']=day.strftime("%B")
			doc_item['year']=each_blog.blog_date.year
			doc_item['day']=each_blog.blog_date.day
			docs.append(doc_item)

			current_blog={}
			current_blog['trunc_data']=''
			text_len=0
			current_blog['blog'] = each_blog
			current_blog['url_title'] = re.sub(' ','-',each_blog.blog_title)
			soup=BeautifulSoup(each_blog.blog_data)
			all_p = soup.find_all('p')
			if len(all_p)>0:
				for each_p in all_p:
					text_len=text_len+len(each_p.get_text())
					dammit = UnicodeDammit(each_p.get_text())
					htext=dammit.unicode_markup
					if text_len > MAX_TEXT_LEN:
						if text_len - MAX_TEXT_LEN > 100:
							current_blog['trunc_data'] +='<p>'+htext[:text_len - MAX_TEXT_LEN]+'</p>'
							break
						else:
							break
					else:
						current_blog['trunc_data'] +='<p>'+htext+'</p>'
				all_img=soup.find_all('img')
				if len(all_img)>0:
					num_images=0
					images_html=''
					for each_img in all_img:
						num_images+=1
						if num_images>MAX_NUM_IMAGES:
							break
						else:
							images_html+='<img src=\''+each_img['src']+'\' class=\'img_thumbnail\' />'
					current_blog['trunc_data'] += '<p>'+images_html+'</p>'
			else:
				all_img = soup.find_all('img')
				images_html=''
				if len(all_img)>0:
					num_images=0
					images_html=''
					for each_img in all_img:
						num_images+=1
						if num_images>MAX_NUM_IMAGES:
							break
						else:
							images_html+='<img src=\''+each_img['src']+'\' class=\'img_thumbnail\' />'
				dammit = UnicodeDammit(soup.get_text())
				htext=dammit.unicode_markup
				
				current_blog['trunc_data'] +='<p>'+htext[:2000]+'</p>'
				current_blog['trunc_data'] += '<p>'+images_html+'</p>'

			all_blogs.append(current_blog)




	return render_to_response('index/templates/main_page.html',
								{
								'all_blogs':all_blogs,
								'docs':docs
								},context_instance=RequestContext(request)
							)
