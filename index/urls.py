from django.conf.urls import patterns, include, url

urlpatterns = patterns('index.views',
    url(r'^$','main_page',name='main_page'),

   url(r'^about/$', 'about',name='about'),
)
