from django.conf.urls import patterns, include, url

urlpatterns = patterns('api.views',
     url(r'^blog_list/$','blog_list',name='blog_list'),
     

    url(r'^blog_short/(?P<blog_id>\d+)\-?(.?)+/$','blog_detail_short',name='blog_detail_short'),
     url(r'^blog_full/(?P<blog_id>\d+)\-?(.?)+/$','blog_detail_full',name='blog_detail_full'),

   # url(r'^about/$', 'about',name='about'),
)
