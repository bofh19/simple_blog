
from django.http import HttpResponse
from blog.models import *

from bs4 import BeautifulSoup
from bs4 import UnicodeDammit
from django.db.models import Q
import json

from django.shortcuts import get_object_or_404

MAX_TEXT_LEN = 2000
MAX_NUM_IMAGES = 4
mimetype = 'application/json'

from django.contrib.auth.decorators import login_required


@login_required(login_url='/accounts/login/')
def blog_detail_short(request, blog_id):
    blog_json = {}

    current_blog = get_object_or_404(blog, ~Q(blog_enabled='0'), id=blog_id)

    blog_json['id'] = current_blog.id
    blog_json['title'] = current_blog.blog_title

    blog_json['trunc_data'] = ''
    text_len = 0
    soup = BeautifulSoup(current_blog.blog_data)
    all_p = soup.find_all('p')
    if len(all_p) > 0:
        for each_p in all_p:
            text_len = text_len + len(each_p.get_text())
            dammit = UnicodeDammit(each_p.get_text())
            htext = dammit.unicode_markup
            if text_len > MAX_TEXT_LEN:
                if text_len - MAX_TEXT_LEN > 100:
                    blog_json['trunc_data'] += '<p>' + htext[:text_len - MAX_TEXT_LEN] + '</p>'
                    break
                else:
                    break
            else:
                blog_json['trunc_data'] += '<p>' + htext + '</p>'
        all_img = soup.find_all('img')
        if len(all_img) > 0:
            num_images = 0
            images_html = ''
            for each_img in all_img:
                num_images += 1
                if num_images > MAX_NUM_IMAGES:
                    break
                else:
                    images_html += '<img src=\'' + each_img['src'] + '\' class=\'img_thumbnail\' />'
            blog_json['trunc_data'] += '<p>' + images_html + '</p>'
    else:
        all_img = soup.find_all('img')
        images_html = ''
        if len(all_img) > 0:
            num_images = 0
            images_html = ''
            for each_img in all_img:
                num_images += 1
                if num_images > MAX_NUM_IMAGES:
                    break
                else:
                    images_html += '<img src=\'' + each_img['src'] + '\' class=\'img_thumbnail\' />'
        dammit = UnicodeDammit(soup.get_text())
        htext = dammit.unicode_markup

        blog_json['trunc_data'] += '<p>' + htext[:2000] + '</p>'
        blog_json['trunc_data'] += '<p>' + images_html + '</p>'

    blog_json['date'] = {}
    blog_json['date']['day'] = current_blog.blog_date.day
    blog_json['date']['month'] = current_blog.blog_date.month
    blog_json['date']['year'] = current_blog.blog_date.year
    result_json = json.dumps(blog_json)
    return HttpResponse(result_json, mimetype)


@login_required(login_url='/accounts/login/')
def blog_detail_full(request, blog_id):
    blog_json = {}

    current_blog = get_object_or_404(blog, ~Q(blog_enabled='0'), id=blog_id)

    blog_json['id'] = current_blog.id
    blog_json['title'] = current_blog.blog_title

    blog_json['trunc_data'] = current_blog.blog_data

    result_json = json.dumps(blog_json)
    return HttpResponse(result_json, mimetype)

