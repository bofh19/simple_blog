
from django.http import HttpResponse
from blog.models import *

import json

from django.db.models import Q

mimetype = 'application/json'

from django.contrib.auth.decorators import login_required


@login_required(login_url='/accounts/login/')
def blog_list(request):
    all_blogs = []

    blog_all = blog.objects.filter(Q(blog_enabled='1'))

    for each_blog in blog_all:
        current_blog = {}
        current_blog['id'] = each_blog.id
        current_blog['title'] = each_blog.blog_title
        all_blogs.append(current_blog)

    result_json = json.dumps(all_blogs)
    return HttpResponse(result_json, mimetype)
