from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import patterns, url, include
# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'ccs.views.home', name='home'),
                       # url(r'^ccs/', include('ccs.foo.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       url(r'^adminx/', include(admin.site.urls)),
                       #url(r'^comments/', include('django.contrib.comments.urls')),

                       url(r'^index/', include('index.urls')),
                       url(r'^blog/', include('blog.urls')),
                       url(r'^$', 'index.views.main_page', name="index"),
                       # url(r'^tinymce/', include('tinymce.urls')),
                        (r'^accounts/', include('allauth.urls')),
                       url(r'^api/', include('api.urls')),
                       )

urlpatterns += patterns('',

                        ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

